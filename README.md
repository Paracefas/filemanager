# FileManager
### *by Emanuel Clur*, a.k.a. @Paracefas.

Is a basic program that watch for a specific path and dispatch the files within that path to user-defined folders. In order to maintain the files classified by its extention, the user have to write a simple .config file. In wich he provides a list of pairs: file extention and destination's directory.

## Configuration

A .config file should looks like this example:


`[watch]=C:\Users\SomeUser\Downloads;`

>*`[watch]` is not an extention but a var that indicates where to keep watching. By default is setted to .\ that is the >same context within the program is running*.

`pdf=C:\Users\SomeUser\Documents;`

>*note that every line have to end with semicolon*.

## Instalation
