#pragma once
#define __DEBUG_MODE 0
#if (__DEBUG_MODE == 1)
#define LOG(msg, x) std::cout << "[\"" << __FILE__ << "\" ("<< __LINE__ << "), '" << msg << "']: " << x << '\n';
#else
#define LOG(msg, x)
#endif
#include <iostream>
#include <vector>
#include <unordered_map>
#include <filesystem>

using FieldValueMp = std::unordered_map<std::string, std::string>;

int handle_error(const std::error_code& error);

// Some little changes but stolen from: https://www.techiedelight.com/split-string-cpp-using-delimiter/
std::vector<std::string> split(std::string const& str, const char delim);

// Trim soled from: https://stackoverflow.com/questions/216823/how-to-trim-a-stdstring
std::string& rtrim(std::string& s, const char* t = " \t\n\r\f\v");

std::string& ltrim(std::string& s, const char* t = " \t\n\r\f\v");

std::string& trim(std::string& s, const char* t = " \t\n\r\f\v");

void moveFile(std::filesystem::path from, std::filesystem::path to);