#pragma once

#include "Utility.h"
#include <efsw/efsw.hpp>

class UpdateListener;
class FileManagerApp
{
    friend class UpdateListener;
    FieldValueMp        _config;
    efsw::FileWatcher   _fileWatcher;
    UpdateListener* _listener;
    bool _restart = false;
    void _init();
public:
    FileManagerApp();
    ~FileManagerApp();
};

FieldValueMp readFromConfig(const std::string& config_file_path);
