#include "UpdateListener.h"
#include "FileManagerApp.h"

UpdateListener::UpdateListener(FileManagerApp* app) : _app{ app }, _config{ app->_config } {}

void UpdateListener::handleFileAction(efsw::WatchID watchid, const std::string& dir, const std::string& filename, efsw::Action action, std::string oldFilename)
{
    switch (action)
    {
    case efsw::Actions::Add:
    {
        std::string dest = _config[_getext(filename)];
        std::filesystem::path path = dir;
        path /= filename;
        if (dest != "")
            moveFile(path, dest);
        break;
    }
    case efsw::Actions::Modified:
    {
        if (filename == ".config")
        {
            _app->_restart = true;
        }
        break;
    }
    case efsw::Action::Delete: break;
    case efsw::Action::Moved: break;
    default: break;
    }
}