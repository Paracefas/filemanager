#pragma once
#include "Utility.h"
// Cross-platform watch library from: https://github.com/SpartanJ/efsw
#include <efsw/efsw.hpp>

class FileManagerApp;
class UpdateListener : public efsw::FileWatchListener
{
    FieldValueMp& _config;
    FileManagerApp* _app;
    std::string _getext(const std::string& file)
    {
        return *(split(file, '.').end() - 1);
    }
public:
    UpdateListener(FileManagerApp* app);
    void handleFileAction(efsw::WatchID watchid, const std::string& dir, const std::string& filename, efsw::Action action, std::string oldFilename) override;
};
