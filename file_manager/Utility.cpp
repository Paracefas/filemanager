#include "Utility.h"

int handle_error(const std::error_code& error)
{
    const auto& errmsg = error.message();
    std::cout << "Error on .config: " << errmsg << "\n";
    return error.value();
}

// Some little changes but stolen from: https://www.techiedelight.com/split-string-cpp-using-delimiter/
std::vector<std::string> split(std::string const& str, const char delim)
{
    std::vector<std::string> out;
    size_t start;
    size_t end = 0;

    while ((start = str.find_first_not_of(delim, end)) != std::string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
    return out;
}

// Trim soled from: https://stackoverflow.com/questions/216823/how-to-trim-a-stdstring
std::string& rtrim(std::string& s, const char* t)
{
    s.erase(s.find_last_not_of(t) + 1);
    return s;
}

std::string& ltrim(std::string& s, const char* t)
{
    s.erase(0, s.find_first_not_of(t));
    return s;
}

std::string& trim(std::string& s, const char* t)
{
    return ltrim(rtrim(s, t), t);
}

void moveFile(std::filesystem::path from, std::filesystem::path to)
{
    std::error_code ec;
    std::filesystem::create_directories(to, ec);
    if (!ec) {
        to = to / from.filename();
        std::filesystem::path base_dir = to.string().substr(0, to.string().find(to.filename().string())-1);
        while (std::filesystem::exists(to)) {
            to = base_dir / ("Copy of " + to.filename().string());
        }
        std::filesystem::copy_file(from, to, ec);
        if (!ec) {
            std::filesystem::remove(from);
        }
    }
    else {
        std::cout << "Error: " << ec.value() << '\n';
    }
}
