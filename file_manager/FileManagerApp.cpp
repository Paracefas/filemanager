#include "FileManagerApp.h"
#include "UpdateListener.h"

// mmap cross-platform stolen from: https://github.com/mandreyel/mio
#include <mio.hpp>

FieldValueMp readFromConfig(const std::string& config_file_path)
{
    std::error_code error;
    mio::mmap_source mmap = mio::make_mmap_source(config_file_path, error);
    if (error) exit(handle_error(error));
    mmap.map(config_file_path, error);
    if (error) exit(handle_error(error));

    FieldValueMp ret;
    std::string data{ mmap.data() };
    std::vector<std::string> lines = split(data, ';');

    // Obtain [extention] = destination;
    for (std::string line : lines) {
        auto v = split(trim(line), '=');
        if (v.size() != 2) return ret;
        auto key = v[0];
        auto value = v[1];
        ret[key] = value;
    }

    return ret;
}

void FileManagerApp::_init()
{
    _restart = false;
    _config = readFromConfig(".config");
    if (_listener) delete _listener;
    _listener = new UpdateListener(this);
    std::string dir = _config["[watch]"];
    LOG("Watch directory", dir)
        LOG(".txt dispatched to", _config["txt"])
        efsw::WatchID watchIDdir = _fileWatcher.addWatch(dir, _listener, false);
    efsw::WatchID watchIDconf = _fileWatcher.addWatch("./", _listener, false);
    LOG("Watching...", _restart)
        while (!_restart) {
            _fileWatcher.watch();
        }
    LOG("Restarting...", _restart)
        _fileWatcher.removeWatch(watchIDconf);
    _fileWatcher.removeWatch(watchIDdir);
    LOG("Done", _restart)
        _init();
}

FileManagerApp::FileManagerApp() : _listener{ new UpdateListener(this) }
{
    _init();
}

FileManagerApp::~FileManagerApp()
{
    delete _listener;
}
